package com.stg.makethon.digisig.controller;

import com.stg.makethon.digisig.crytpo.DigiSignature;
import com.stg.makethon.digisig.crytpo.ECDSADigiSignature;
import com.stg.makethon.digisig.entity.SignedDataDto;
import com.stg.makethon.digisig.service.DigitalSignatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class SignatureController {

    private DigitalSignatureService signatureService;

    @Autowired
    public SignatureController(DigitalSignatureService signatureService) {
        this.signatureService = signatureService;
    }

    @GetMapping("/api/greet")
    public ResponseEntity<String> greet() {
        return new ResponseEntity<>("Hello, World", HttpStatus.OK);
    }

    @GetMapping("/api/digisig/keys/new")
    public ResponseEntity generateKeys() {
        DigiSignature digiSignature = new ECDSADigiSignature();
        digiSignature.generateKeys();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/api/digisig/signature")
    public ResponseEntity<SignedDataDto> sign(@RequestBody Object data) {
        String signature = signatureService.signData(data);
        return new ResponseEntity<>(new SignedDataDto(data, signature), HttpStatus.OK);
    }

    @PostMapping("/api/digisig/verification")
    public ResponseEntity<Boolean> verify(@RequestBody SignedDataDto dataDto) {
        boolean isVerified = signatureService.verifySignature(dataDto.getData(), dataDto.getSignature());
        return new ResponseEntity<>(isVerified, HttpStatus.OK);
    }
}
