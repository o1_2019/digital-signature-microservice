package com.stg.makethon.digisig.service;

import com.stg.makethon.digisig.crytpo.DigiSignature;
import com.stg.makethon.digisig.crytpo.ECDSADigiSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DigitalSignatureService {

    private DigiSignature digiSignature;

    @Autowired
    public DigitalSignatureService() {
        this.digiSignature = new ECDSADigiSignature();
    }


    public String signData(Object data) {
        return digiSignature.sign(data.toString());
    }

    public boolean verifySignature(Object data, String signature) {
        return digiSignature.verify(data.toString(), signature);
    }

}
