package com.stg.makethon.digisig;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.Security;

@SpringBootApplication
public class DigiSigApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigiSigApplication.class, args);
        Security.addProvider(new BouncyCastleProvider());
    }
}
