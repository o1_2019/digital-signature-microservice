package com.stg.makethon.digisig.entity;

import java.util.Map;

public class SignedDataDto {
    private Object data;
    private String signature;

    public SignedDataDto(Object data, String signature) {
        this.data = data;
        this.signature = signature;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "SignedDataDto{" +
                "data='" + data.toString() + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }
}
