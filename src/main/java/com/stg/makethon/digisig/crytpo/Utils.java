package com.stg.makethon.digisig.crytpo;

import java.io.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class Utils {
    static void saveToFile(String fileName, String data) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static PrivateKey readPrivateKey(String filename) {
        PrivateKey privateKey = null;
        try {
            String base64key = readFromFile(filename);
            KeyFactory keyFactory = KeyFactory.getInstance("ECDSA", "BC");
            privateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64key)));
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return privateKey;
    }

    static PublicKey readPublicKey(String filename) {
        PublicKey publicKey = null;
        try {
            String base64key = readFromFile(filename);
            KeyFactory keyFactory = KeyFactory.getInstance("ECDSA", "BC");
            publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(base64key)));
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    private static String readFromFile(String filename) {
        String base64Key = null;
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            base64Key = reader.readLine();
            System.out.println(base64Key);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return base64Key;
    }
}
