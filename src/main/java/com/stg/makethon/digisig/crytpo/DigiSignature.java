package com.stg.makethon.digisig.crytpo;

public interface DigiSignature {
    void generateKeys();

    String sign(String data);

    boolean verify(String data, String signature);
}
