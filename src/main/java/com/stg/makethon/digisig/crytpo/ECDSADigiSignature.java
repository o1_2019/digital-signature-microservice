package com.stg.makethon.digisig.crytpo;

import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.spec.ECParameterSpec;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Base64;

public class ECDSADigiSignature implements DigiSignature {
    private static final String PUB_KEY = "public.xxx";
    private static final String PRIVATE_KEY = "private.xxx";

    private Signature getEcdsaInstance() throws NoSuchProviderException, NoSuchAlgorithmException {
        return Signature.getInstance("SHA256withECDSA", "BC");
    }

    @Override
    public void generateKeys() {
        try {
            ECParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec("B-571");
            KeyPairGenerator g = KeyPairGenerator.getInstance("ECDSA", "BC");
            g.initialize(ecSpec, new SecureRandom());
            KeyPair keypair = g.generateKeyPair();
            String publicKey = Base64.getEncoder().encodeToString(keypair.getPublic().getEncoded());
            String privateKey = Base64.getEncoder().encodeToString(keypair.getPrivate().getEncoded());
            System.out.println("PUBLIC: " + publicKey);
            Utils.saveToFile(PUB_KEY, publicKey);
            System.out.println("PRIVATE: " + privateKey);
            Utils.saveToFile(PRIVATE_KEY, privateKey);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String sign(String data) {
        String signature = null;
        try {
            System.out.println("Signing: " + data);
            Signature ecdsaSign = getEcdsaInstance();
            ecdsaSign.initSign(getPrivateKey());
            ecdsaSign.update(data.getBytes(StandardCharsets.UTF_8));
            byte[] signBytes = ecdsaSign.sign();
            signature = Base64.getEncoder().encodeToString(signBytes);
            System.out.println(signature);
        } catch (InvalidKeyException | SignatureException | NoSuchAlgorithmException | NoSuchProviderException e) {
            e.printStackTrace();
        }
        return signature;
    }

    @Override
    public boolean verify(String data, String signature) {
        boolean isVerified = false;
        try {
            Signature ecdsaSign = getEcdsaInstance();
            ecdsaSign.initVerify(getPublicKey());
            ecdsaSign.update(data.getBytes(StandardCharsets.UTF_8));
            isVerified = ecdsaSign.verify(Base64.getDecoder().decode(signature));
        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException | SignatureException e) {
            e.printStackTrace();
        }
        return isVerified;
    }

    private PrivateKey getPrivateKey() {
        return Utils.readPrivateKey(PRIVATE_KEY);
    }

    private PublicKey getPublicKey() {
        return Utils.readPublicKey(PUB_KEY);
    }
}
